#include "navigationcontrols.h"
#include "imgui/imgui.h"

NavigationControls::NavigationControls(GLFWwindow *window, Camera *camera):Controls(window, camera), lastPosCursor(-1,-1)
{
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    mouseSpeed = 0.02;
}

void NavigationControls::update(float deltaTime, Shader *shader)
{


    // On test d'abord si on est sur la fenêtre imgui ou pas
    if (/*!io.WantCaptureMouse*/true){

        int state = glfwGetMouseButton(m_Window, GLFW_MOUSE_BUTTON_LEFT);
        double xpos, ypos;
        glfwGetCursorPos(m_Window, &xpos, &ypos);
        if(state == GLFW_PRESS){
            if (lastPosCursor.x!=-1){
                m_Camera->horizontalAngle+= mouseSpeed * deltaTime * float( xpos-lastPosCursor.x );
                m_Camera->verticalAngle  += mouseSpeed * deltaTime * float( ypos-lastPosCursor.y );
            }
            lastPosCursor.x = xpos;
            lastPosCursor.y = ypos;
        }
        else{
           lastPosCursor = glm::vec2(-1,-1);
        }


        glm::vec3 direction(
            sin(m_Camera->horizontalAngle),
            0.0f,
            //sin(m_Camera->verticalAngle),
            cos(m_Camera->horizontalAngle)
        );

        // Right vector
        glm::vec3 right = glm::vec3(
            sin(m_Camera->horizontalAngle - 3.14f/2.0f),
            0,
            cos(m_Camera->horizontalAngle - 3.14f/2.0f)
        );

        // Up vector : perpendicular to both direction and right
        glm::vec3 up = glm::cross( right, direction );


        // Move forward
        if (glfwGetKey(m_Window, GLFW_KEY_UP ) == GLFW_PRESS){
            m_Camera->position += direction * deltaTime * speed;
        }
        // Move backward
        if (glfwGetKey(m_Window, GLFW_KEY_DOWN ) == GLFW_PRESS){
            m_Camera->position -= direction * deltaTime * speed;
        }
        // Strafe right
        if (glfwGetKey(m_Window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
            m_Camera->position += right * deltaTime * speed;
        }
        // Strafe left
        if (glfwGetKey(m_Window, GLFW_KEY_LEFT ) == GLFW_PRESS){
            m_Camera->position -= right * deltaTime * speed;
        }
        // Move forward
        if (glfwGetKey(m_Window, GLFW_KEY_W ) == GLFW_PRESS){
            m_Camera->position += direction * deltaTime * speed;
        }
        // Move backward
        if (glfwGetKey(m_Window, GLFW_KEY_S ) == GLFW_PRESS){
            m_Camera->position -= direction * deltaTime * speed;
        }
        // Strafe right
        if (glfwGetKey(m_Window, GLFW_KEY_D ) == GLFW_PRESS){
            m_Camera->position += right * deltaTime * speed;
        }
        // Strafe left
        if (glfwGetKey(m_Window, GLFW_KEY_A ) == GLFW_PRESS){
            m_Camera->position -= right * deltaTime * speed;
        }


//        // go up
//        if (glfwGetKey(m_Window, GLFW_KEY_SPACE ) == GLFW_PRESS){
//            m_Camera->position += up * deltaTime * speed;
//        }
//        // go down
//        if (glfwGetKey(m_Window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS){
//            m_Camera->position -= up * deltaTime * speed;
//        }
    }

}

void NavigationControls::stayOnDTM(std::vector<std::vector<float>> hauteurs)
{
    //Ne pas tomber des bords du monde
    if (m_Camera->position.x < 1) {
        m_Camera->position.x = 1;
    } else if (m_Camera->position.x > hauteurs.size() -2) {
        m_Camera->position.x = hauteurs.size() -2;
    }
    if (m_Camera->position.z < 1) {
        m_Camera->position.z = 1;
    } else if (m_Camera->position.z > hauteurs.size() -2){
        m_Camera->position.z = hauteurs.size() -2;
    }
    //Ne pas se prendre le mnt
    if (m_Camera->position.x + m_Camera->position.z < floor(m_Camera->position.x) + floor(m_Camera->position.z) +1) {
        //Ici, on est dans le triangle haut gauche de notre carré
        glm::vec3 u = {1.0f, hauteurs[floor(m_Camera->position.z)][floor(m_Camera->position.x) +1] - hauteurs[floor(m_Camera->position.z)][floor(m_Camera->position.x)], 0.0f};
        glm::vec3 v = {0.0f, hauteurs[floor(m_Camera->position.z) +1][floor(m_Camera->position.x)] - hauteurs[floor(m_Camera->position.z)][floor(m_Camera->position.x)], 1.0f};
        glm::vec3 n = {u[1], -1.0f, v[1]};
        m_Camera->position.y = (m_Camera->position.x - floor(m_Camera->position.x))*n[0] + (m_Camera->position.z - floor(m_Camera->position.z))*n[2] + hauteurs[floor(m_Camera->position.z)][floor(m_Camera->position.x)] + 1;
    } else {
        //Ici, on est dans le triangle bas droite de notre carré
        glm::vec3 u = {-1.0f, hauteurs[floor(m_Camera->position.z) +1][floor(m_Camera->position.x)] - hauteurs[floor(m_Camera->position.z) +1][floor(m_Camera->position.x) +1], 0.0f};
        glm::vec3 v = {0.0f, hauteurs[floor(m_Camera->position.z)][floor(m_Camera->position.x) +1] - hauteurs[floor(m_Camera->position.z) +1][floor(m_Camera->position.x) +1], -1.0f};
        glm::vec3 n = {-u[1], -1.0f, -v[1]};
        m_Camera->position.y = (m_Camera->position.x - floor(m_Camera->position.x) -1)*n[0] + (m_Camera->position.z - floor(m_Camera->position.z) -1)*n[2] + hauteurs[floor(m_Camera->position.z) +1][floor(m_Camera->position.x) +1] + 1;
    }
}
