#include "object.h"

#include <iostream>
#include "renderer.h"
#include "glm/gtx/transform.hpp"

#include <exception>


Object::Object(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::string texturePath):m_vb(0), m_uvsb(0), m_texture(0), position(0,0,0), rotationAngles(0,0,0)
{
     m_vb = new VertexBuffer(vertices);
     m_uvsb = new UVBuffer(uvs);
     m_texture = new Texture(texturePath);
}
//Object::Object(const char* path, std::string texturePath):m_vb(0), m_uvsb(0), m_texture(0), position(0,0,0), rotationAngles(0,0,0)
//{
//    m_texture = new Texture(texturePath);
//    bool loadOBJ(char* path,
//            std::vector<glm::vec3> &out_vertices,
//            std::vector<glm::vec2> &out_uvs,
//            std::vector<glm::vec3> &out_normals
//            );
//}
//bool Object::loadOBJ(char *path, std::vector<glm::vec3> &out_vertices, std::vector<glm::vec2> &out_uvs, std::vector<glm::vec3> &out_normals)
//{
//    std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
//    std::vector< glm::vec3 > temp_vertices;
//    std::vector< glm::vec2 > temp_uvs;
//    std::vector< glm::vec3 > temp_normals;

//    FILE * file = fopen(path, "r");
//    if( file == NULL ){
//        printf("Impossible to open the file !\n");
//        return false;
//    }

//    while( 1 ){

//        char lineHeader[128];
//        // read the first word of the line
//        int res = fscanf(file, "%s", lineHeader);
//        if (res == EOF)
//            break; // EOF = End Of File. Quit the loop.

//        // else : parse lineHeader
//        if ( strcmp( lineHeader, "v" ) == 0 ){
//            glm::vec3 vertex;
//            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
//            temp_vertices.push_back(vertex);
//        }
//        else if ( strcmp( lineHeader, "vt" ) == 0 ){
//            glm::vec2 uv;
//            fscanf(file, "%f %f\n", &uv.x, &uv.y );
//            temp_uvs.push_back(uv);
//        }
//        else if ( strcmp( lineHeader, "vn" ) == 0 ){
//            glm::vec3 normal;
//            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
//            temp_normals.push_back(normal);
//        }
//        else if ( strcmp( lineHeader, "f" ) == 0 ){
//            std::string vertex1, vertex2, vertex3;
//            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
//            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2] );
//            if (matches != 9){
//                printf("File can't be read by our simple parser : ( Try exporting with other options\n");
//                return false;
//            }
//            vertexIndices.push_back(vertexIndex[0]);
//            vertexIndices.push_back(vertexIndex[1]);
//            vertexIndices.push_back(vertexIndex[2]);
//            uvIndices.push_back(uvIndex[0]);
//            uvIndices.push_back(uvIndex[1]);
//            uvIndices.push_back(uvIndex[2]);
//            normalIndices.push_back(normalIndex[0]);
//            normalIndices.push_back(normalIndex[1]);
//            normalIndices.push_back(normalIndex[2]);
//        }
//    }

//    for( unsigned int i=0; i < vertexIndices.size(); i++ ){
//        unsigned int vertexIndex = vertexIndices[i];
//        glm::vec3 vertex = temp_vertices[ vertexIndex-1 ];
//        out_vertices.push_back(vertex);
//    }
//    for( unsigned int i=0; i < uvIndices.size(); i++ ){
//        unsigned int uvIndex = uvIndices[i];
//        glm::vec2 uv = temp_uvs[ uvIndex-1 ];
//        out_uvs.push_back(uv);
//    }
//    for( unsigned int i=0; i < normalIndices.size(); i++ ){
//        unsigned int normalIndex = normalIndices[i];
//        glm::vec3 normal = temp_normals[ normalIndex-1 ];
//        out_normals.push_back(normal);
//    }

//    return true;
//}

Object::~Object()
{
    delete m_vb;
    if (m_uvsb) delete m_uvsb;
    if (m_texture) delete m_texture;
}

void Object::Bind() const
{
    m_vb->Bind(0);
    if (m_uvsb) m_uvsb->Bind(1);
    if (m_texture) m_texture->Bind(0);
}

void Object::Unbind() const
{
    m_vb->Unbind();
    if (m_uvsb) m_uvsb->Unbind();
    if (m_texture) m_texture->Unbind();
}



void Object::Draw() const
{
    GLCall(glDrawArrays(GL_TRIANGLES,0, m_vb->getSize()));
}

glm::mat4 Object::getModelMatrix()
{
    glm::mat4 m = glm::rotate(glm::translate(glm::mat4(1), position), rotationAngles.x, glm::vec3(1,0,0));
    m=glm::rotate(m, rotationAngles.y, glm::vec3(0,1,0));
    m=glm::rotate(m, rotationAngles.z, glm::vec3(0,0,1));
    return m;
}

//Fonction qui crée une liste de hauteurs aléatoires puis qui les lisse.
std::vector<std::vector<float>> rand_heights(int taille)
{
    //Hauteurs aléatoires. Le tableau est trop grand de 2 cases, pour le lissage.
    std::vector<std::vector<float>> hauteurs;
    for(int l=0; l<taille+4; l++){
        std::vector<float> ligne;
        for(int c=0; c<taille+4; c++){
            ligne.push_back((rand()%11)/10.0 *10);
        }
        hauteurs.push_back(ligne);
    }

    //lissage des hauteurs
    for(int c=2; c<taille+2; c++){
        for(int l=2; l<taille+2; l++){
            //moyenne
            float moy = 0;
            for(int i=-2; i<3; i++){
                for(int j=-2; j<3; j++){
                    moy += hauteurs.at(l+i).at(c+j);
                }
            }
            moy /= 25;
            hauteurs.at(l).at(c) = moy;
            /*Oui, on va se retrouver à ajouter dans notre moyenne des hauteurs qu'on a déjà moyennées.
            Ce n'était pas mon objectif au départ, mais comme le résultat m'a bien plu, cette erreur est désormais une feature.
            Je trouve que ça donne l'impression d'un côté du MNT plus montagneux que le reste.*/
        }
    }

    //Enlever les éléments de trop, qui étaient nécessaires pour le lissage.
    std::vector<std::vector<float>> res;
    for(int l=2; l<taille+2; l++){
        std::vector<float> ligne;
        for(int c=2; c<taille+2; c++){
            ligne.push_back(hauteurs.at(l).at(c));
        }
        res.push_back(ligne);
    }

    return res;
}

//Fonction qui crée la liste de points nécessaire à la création du vertex buffer de l'object.
std::vector<glm::vec3> vb_mnt(int taille, std::vector<std::vector<float>> hauteurs)
{
    //création des triangles
    std::vector<glm::vec3> res;
    for(int c=0; c<taille; c++){
        for(int l=0; l<taille; l++){
            if(c!=0 && l!=0){
                //Ici notre point n'est collé ni à la paroi du haut, ni à celle de gauche, et on fait donc son triangle haut gauche (donc il est le coin de l'angle droit).

                res.push_back({(float) c, hauteurs.at(l).at(c), (float) l});
                res.push_back({(float) c-1, hauteurs.at(l).at(c-1), (float) l});
                res.push_back({(float) c, hauteurs.at(l-1).at(c), (float) l-1});
            }
            if( c!=taille-1  && l!=taille-1){
                //Ici notre point n'est collé ni à la paroi du bas, ni à celle de droite, et on fait donc son triangle bas droite (donc il est le coin de l'angle droit).

                res.push_back({(float) c, hauteurs.at(l).at(c), (float) l});
                res.push_back({(float) c+1, hauteurs.at(l).at(c+1), (float) l});
                res.push_back({(float) c, hauteurs.at(l+1).at(c), (float) l+1});
            }
        }
    }
    return res;
}

//Fonction qui crée la liste de points nécessaire à la création du uv buffer de l'object.
std::vector<glm::vec2> uvb_mnt(int taille)
{
    std::vector<glm::vec2> res;
    for(int c=0; c<taille; c++){
        for(int l=0; l<taille; l++){
            if(c!=0 && l!=0){
                //Ici notre point n'est collé ni à la paroi du haut, ni à celle de gauche, et on fait donc son triangle haut gauche (donc il est le coin de l'angle droit).

                res.push_back(glm::vec2(1.0f, 1.0f));
                res.push_back(glm::vec2(0.0f, 1.0f));
                res.push_back(glm::vec2(1.0f, 0.0f));
            }
            if( c!=taille-1  && l!=taille-1){
                //Ici notre point n'est collé ni à la paroi du bas, ni à celle de droite, et on fait donc son triangle bas droite (donc il est le coin de l'angle droit).

                res.push_back(glm::vec2(0.0f, 0.0f));
                res.push_back(glm::vec2(1.0f, 0.0f));
                res.push_back(glm::vec2(0.0f, 1.0f));
            }
        }
    }
    return res;
}

//Fonction qui gère un déplacement latéral sur l'axe x d'un object (dont le centre se trouve, de préférence, à 0.866025 uu du sol).
int Object::bougeTonCube(int sens, double deltaTemps, std::vector<std::vector<float>> hauteurs)
{
    //Déplacement
    position.x += sens * deltaTemps;

    //Ne pas se prendre le mnt (enfin pas trop)
    if (position.x + position.z < floor(position.x) + floor(position.z) +1) {
        //Ici, on est dans le triangle haut gauche de notre carré de sol.
        glm::vec3 u = {1.0f, hauteurs[floor(position.z)][floor(position.x) +1] - hauteurs[floor(position.z)][floor(position.x)], 0.0f};
        glm::vec3 v = {0.0f, hauteurs[floor(position.z) +1][floor(position.x)] - hauteurs[floor(position.z)][floor(position.x)], 1.0f};
        glm::vec3 n = {u[1], -1.0f, v[1]};
        position.y = (position.x - floor(position.x))*n[0] + (position.z - floor(position.z))*n[2] + hauteurs[floor(position.z)][floor(position.x)] + 0.866025f;
    } else {
        //Ici, on est dans le triangle bas droite de notre carré de sol.
        glm::vec3 u = {-1.0f, hauteurs[floor(position.z) +1][floor(position.x)] - hauteurs[floor(position.z) +1][floor(position.x) +1], 0.0f};
        glm::vec3 v = {0.0f, hauteurs[floor(position.z)][floor(position.x) +1] - hauteurs[floor(position.z) +1][floor(position.x) +1], -1.0f};
        glm::vec3 n = {-u[1], -1.0f, -v[1]};
        position.y = (position.x - floor(position.x) -1)*n[0] + (position.z - floor(position.z) -1)*n[2] + hauteurs[floor(position.z) +1][floor(position.x) +1] + 0.866025f;
    }

    //Ne pas tomber des bords du monde
    if (position.x < 1) {
        sens = 1;
    } else if (position.x > hauteurs.size() -2) {
        sens = -1;
    }
    return sens;
}
