#ifndef OBJECT_H
#define OBJECT_H

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <iostream>
#include "vertexbuffer.h"
#include "vertexarray.h"
#include "texture.h"
#include "uvbuffer.h"
#include "string.h"

class Object
{
public:
    Object(std::vector< glm::vec3 > vertices, std::vector< glm::vec2 > uvs, std::string texturePath);
    //Object(const char *path,std::string texturePath);
    ~Object();
    void Bind() const;
    void Unbind() const;
    void Draw() const;
    int bougeTonCube(int sens, double deltaTemps, std::vector<std::vector<float>> hauteurs);
    glm::vec3 position;
    glm::vec3 rotationAngles;
    glm::mat4 getModelMatrix();

private:
    VertexBuffer *m_vb;
    UVBuffer *m_uvsb;
    Texture *m_texture;
    //bool loadOBJ(char*,std::vector<glm::vec3>, std::vector<glm::vec2>, std::vector<glm::vec3>);


};

std::vector<std::vector<float>> rand_heights(int taille);
std::vector<glm::vec3> vb_mnt(int taille, std::vector<std::vector<float>> hauteurs);
std::vector<glm::vec2> uvb_mnt(int taille);
#endif // OBJECT_H
